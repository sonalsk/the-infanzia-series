# THE INFANZIA SERIES 1 : Know-It-Kid


**“Know-It-Kid”** is an interactive learning mobile application for Pre-Schoolers and Kindergartners. This app is one among the Infanzia Series Applications. The main idea behind this app is to create a fun and interactive genereal environment for kids.

<img src="https://cdn.leverageedu.com/blog/wp-content/uploads/2020/02/08152551/General-Knowledge-for-Kids.jpg" width="30%">

### DIVISION OF CONTENT BY CATEGORY

To make the learning process smoother, we have decided to implement content division by category. 
The 3 categories are : General & Environmental Knowledge, Games, Rhymes.
At the time of learning, the child will be able to choose and switch between the three learning paths anytime.



### DIVISION OF CONTENT : PRE-SCHOOLERS AND KINDERGARTNERS

Based on the age group, we have divided the content into two categories, i.e., Pre-Schoolers and Kindergartners.  The content, including the exercises, will be designed in such a way that it goes well with the child and his age.

### EDUCATIONAL CONTENT

> **GROUP 1 : PRE-SCHOOLERS**

<img src="https://www.woodlandstreehouse.com/site/wp-content/uploads/2018/04/preschool-art.jpg" width="30%">

Children are at the very basic stage of their learning and would have started with simple knowledge of the environment. The content is designed in such a way so that they can get started easily.

The topics include : 
1. **Introduction**
This is a part of teaching students the basic sentence structure by teaching them to introduce themselves . They will be taught to tell basic information around them to people around them.
 
2. **Colors**
The purpose of this is to teach different types of colors and matching.
<img src="https://i.ytimg.com/vi/BGa3AqeqRy0/hqdefault.jpg" width = "200">

3. **Shapes**
The purpose of this is to teach different types of Shapes and objects of different shapes.
<img src="https://d138zd1ktt9iqe.cloudfront.net/media/seo_landing_files/file-shapes-for-kids-1592568510.jpg" width = "200">

4. **Fruits**
The purpose of this is to teach different types of fruits and identification.
<img src="https://i.ytimg.com/vi/S0S1M9bKlYk/maxresdefault.jpg" width = "200">

5. **Vegetables**
The purpose of this is to teach different types of vegetables and identification.
<img src="https://lh3.googleusercontent.com/proxy/1iecL1yUVtjHJ8PanplrKhgNT_2H-0zSDcHgGW1k4FVPrjSMNZ30qj-eQDTkHCWz0ylCxBBP1IMuL1avXbb6sjzm_m-nn6fy" width = "200">

6. **Body Parts**
The purpose of this is to teach different body parts.
<img src="https://media.istockphoto.com/vectors/cute-girl-body-parts-vector-id1214084598" width = "200">




> **GROUP 2 : KINDERGARTNERS**

<img src="https://www.sheknows.com/wp-content/uploads/2018/08/kindergarten-students-feature.jpeg?w=695&h=391&crop=1" width = "200">

The topics in this module include : 
1. **Introduction**
This is a part of teaching students the basic sentence structure by teaching them to introduce themselves . They will be taught to tell basic information around them to people around them.

2. **Seasons**
The purpose of this is to teach different types of seasons and the objects used in different seasons.
<img src="https://i.pinimg.com/originals/7a/09/e8/7a09e89c9815934cab70a79a315c3db6.jpg" width = "200">

3. **Animals**
The purpose of this is to teach different types of Animals and their classification in wild, domestic and pet.
<img src="https://i.etsystatic.com/7264394/r/il/dc4dc8/1695624807/il_570xN.1695624807_h8n4.jpg" width = "200">

4. **Professions**
The purpose of this is to teach different types of professions which will make them think about what they can be.
<img src="https://previews.123rf.com/images/virinka/virinka1309/virinka130900030/22095314-cartoon-characters-of-different-professions.jpg" width = "200">

5. **Traffic Rules**
The purpose of this is to teach different traffic rules.
<img src="https://www.cars24.com/blog/wp-content/uploads/2020/02/resize-15825205842021960572RoadSafetyRulesinIndia1.jpg" width = "200">

6. **Habits**
The purpose of this is to teach them good habits and make them aware about the bad habits.
<img src="https://images-na.ssl-images-amazon.com/images/I/91M2jmxYNXL._SL1500_.jpg" width = "200">


<hr>
