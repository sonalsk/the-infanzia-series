
var SPList=[
  {"alpha" : "Images/sp/ant.png", "I" : "Images/sp/ants.png"},
  {"alpha" : "Images/sp/apple.png", "I" : "Images/sp/apples.png"},
  {"alpha" : "Images/sp/box.png", "I" : "Images/sp/boxes.png"},
  {"alpha" : "Images/sp/cat.png", "I" : "Images/sp/cats.png"},
  {"alpha" : "Images/sp/hat.png", "I" : "Images/sp/hat.png"},
];


var SimilarList=[
  {"alpha" : "Images/similar/big.png", "I" : "Images/similar/large.png"},
  {"alpha" : "Images/similar/fast.png", "I" : "Images/similar/speedy.png"},
  {"alpha" : "Images/similar/grin.png", "I" : "Images/similar/smile.png"},
  {"alpha" : "Images/similar/sad.png", "I" : "Images/similar/unhappy.png"},
  {"alpha" : "Images/similar/small.png", "I" : "Images/similar/tiny.png"},
];

var OppositeList=[
  {"alpha" : "Images/Opposite/full.png", "I" : "Images/Opposite/empty.png"},
  {"alpha" : "Images/Opposite/old.png", "I" : "Images/Opposite/young.png"},
  {"alpha" : "Images/Opposite/fast.png", "I" : "Images/Opposite/slow.png"},
  {"alpha" : "Images/Opposite/light.png", "I" : "Images/Opposite/dark.png"},
  {"alpha" : "Images/Opposite/happy.png", "I" : "Images/Opposite/sad.png"},
];

var AlphaList=[
  {"alpha" : "A", "I" : "Images/ant.png"},
  {"alpha" : "B", "I" : "Images/Bee.png"},
  {"alpha" : "C", "I" : "Images/cat.png"},
  {"alpha" : "D", "I" : "Images/dog.png"},
  {"alpha" : "E", "I" : "Images/eagle.png"},
  {"alpha" : "F",  "I" : "Images/fish.png"},
  {"alpha" : "G",  "I" : "Images/girrafe.png"},
  {"alpha" : "H",  "I" : "Images/Horse.png"},
  {"alpha" : "I",  "I" : "Images/igloo.png"},
  {"alpha" : "J",  "I" : "Images/Jackal.png"},
  {"alpha" : "K",  "I" : "Images/Kangaroo.png"},
  {"alpha" : "L",  "I" : "Images/lion.png"},
  {"alpha" : "M",  "I" : "Images/monkey.png"},
  {"alpha" : "N",  "I" : "Images/nest.png"},
  {"alpha" : "O",  "I" : "Images/owl.png"},
  {"alpha" : "P",  "I" : "Images/peacock.png"},
  {"alpha" : "Q",  "I" : "Images/queen.png"},
  {"alpha" : "R",  "I" : "Images/rat.png"},
  {"alpha" : "S",  "I" : "Images/snake.png"},
  {"alpha" : "T",  "I" : "Images/toy.png"},
  {"alpha" : "U",  "I" : "Images/Umbrella.png"},
  {"alpha" : "V",  "I" : "Images/violin.png"},
  {"alpha" : "W",  "I" : "Images/watch.png"},
  {"alpha" : "X",  "I" : "Images/xylophone.png"},
  {"alpha" : "Y",  "I" : "Images/yak.png"},
  {"alpha" : "Z",  "I" : "Images/zebra.png"},

];

