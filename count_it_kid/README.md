# THE INFANZIA SERIES 2 : count-It-Kid


**“Count-It-Kid”** is an interactive learning mobile application for Pre-Schoolers and Kindergartners. This app is one among the Infanzia Series Applications. The main idea behind this app is to create a fun and interactive language learning environment for kids.

<img src="https://static.vecteezy.com/system/resources/previews/000/301/191/original/children-learning-math-with-abacus-vector.jpg" width="30%">

### DIVISION OF CONTENT BY CATEGORY

To make the learning process smoother, we have decided to implement content division by category. 
The 3 categories are : Language, Games, Rhymes.
At the time of learning, the child will be able to choose and switch between the three learning paths anytime.



### DIVISION OF CONTENT : PRE-SCHOOLERS AND KINDERGARTNERS

Based on the age group, we have divided the content into two categories, i.e., Pre-Schoolers and Kindergartners.  The content, including the exercises, will be designed in such a way that it goes well with the child and his age.

### EDUCATIONAL CONTENT

> **GROUP 1 : PRE-SCHOOLERS**

<img src="https://www.woodlandstreehouse.com/site/wp-content/uploads/2018/04/preschool-art.jpg" width="30%">

Children are at the very basic stage of their learning and would have started with simple maths. The content is designed in such a way so that they can get started easily.

The topics include : 
1. **Numonary**
This is a basically a number dictionary with images to teach kids the concept of numbers and counting in a fun and interactive manner
 
2. **Greater Than**
The purpose of this is to teach basic comparisons. It will teach them the use of comparitive operator.
In this case to teach them if a quantity is greater than other 
<img src="https://clipartart.com/images/less-than-clipart-5.png" width = "200">

3. **Less than**
The purpose of this is to teach basic comparisons. It will teach them the use of comparitive operator.
In this case to teach them if a quantity is less than other.


4. **Equal to**
The purpose of this is to teach basic comparisons. It will teach them the use of comparitive operator.
In this case to teach them if a quantity is equal other
<img src="https://th.bing.com/th/id/OIP.znaQxDvSeaKU2F0xmet3BQHaHa?w=199&h=199&c=7&o=5&pid=1.7" width = "200">

5. **Tracing**
The purpose of this is to teach children about basic tracing techniques. It will help them in writing later on.A whiteboard is also provided with this so children can practice tracing.

6. **Exercise**
Fun to do exercise are provided alongside the content , so they can be easily accessed.These are made in such a manner so that the child can gain maximum benefit out of it.




> **GROUP 2 : KINDERGARTNERS**

<img src="https://www.sheknows.com/wp-content/uploads/2018/08/kindergarten-students-feature.jpeg?w=695&h=391&crop=1" width = "200">

Kids of this group are capable of learning advanced Maths . So for them we have decided on a simple quiz and content appropriate for their age .

The topics include : 
1. **Number names** 
The purpose of this is to teach students  o spell ou various numbers in a fun and attractive manner.It is made visually attractive, so it can easily attract children 

2. **Addition**
The purpose is to teach children basic addition, so they can easily understand the concept of adding two quantities in a fun and interactive manner.
<img src="https://th.bing.com/th/id/OIP.RNDqQE-YvMr8cNfdaFEfIgHaFy?w=202&h=180&c=7&o=5&pid=1.7" width = "200">

3. **Subtraction**
The purpose is to teach children basic subtraction, so they can easily understand the concept of subtracting two quantities in a fun and interactive manner.
<img src="http://clipground.com/images/subtraction-clipart-11.jpg" width = "200">

<hr>
