import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math';


var AlphaList=[
  {"alpha" : "Legs", "I" : "Images/Bodyparts/legs.png"},
  {"alpha" : "Hand", "I" : "Images/Bodyparts/Hand.png"},
  {"alpha" : "Mouth", "I" : "Images/Bodyparts/Mouth.png"},
  {"alpha" : "Hair", "I" : "Images/Bodyparts/Hair.png"},
];

var ShapesList=[
  {"alpha" : "Images/Shapes/Circle.png", "I" : "Images/Shapes/circle1.png"},
  {"alpha" : "Images/Shapes/rectangle.png", "I" : "Images/Shapes/rectangle1.png"},
  {"alpha" : "Images/Shapes/square.png", "I" : "Images/Shapes/Square1.png"},
  {"alpha" : "Images/Shapes/Triangle1.png", "I" : "Images/Shapes/triangle.png"},
];

var ProffList=[
  {"alpha" : "Astronaut", "I" : "Images/Profession/astronaut1.png"},
  {"alpha" : "Doctor", "I" : "Images/Profession/doctor1.png"},
  {"alpha" : "Firefighter", "I" : "Images/Profession/firefighter1.png"},
  {"alpha" : "Teacher", "I" : "Images/Profession/Teacher1.png"},
];
