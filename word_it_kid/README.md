# THE INFANZIA SERIES 1 : Word-It-Kid


**“Word-It-Kid”** is an interactive learning mobile application for Pre-Schoolers and Kindergartners. This app is one among the Infanzia Series Applications. The main idea behind this app is to create a fun and interactive language learning environment for kids.

<img src="https://previews.123rf.com/images/sbego/sbego1202/sbego120200017/12352602-children-learning-the-alphabet.jpg" width="30%">

### DIVISION OF CONTENT BY CATEGORY

To make the learning process smoother, we have decided to implement content division by category. 
The 3 categories are : Language, Games, Rhymes.
At the time of learning, the child will be able to choose and switch between the three learning paths anytime.

<img src="https://store-images.s-microsoft.com/image/apps.38274.9007199266810839.a923641b-4cbe-481d-888d-219eb9672666.50ee15da-39af-44e8-a499-4de2eae012d5?mode=scale&q=90&h=1080&w=1920" width="30%">


### DIVISION OF CONTENT : PRE-SCHOOLERS AND KINDERGARTNERS

Based on the age group, we have divided the content into two categories, i.e., Pre-Schoolers and Kindergartners.  The content, including the exercises, will be designed in such a way that it goes well with the child and his age.

### EDUCATIONAL CONTENT

> **GROUP 1 : PRE-SCHOOLERS**

<img src="https://www.woodlandstreehouse.com/site/wp-content/uploads/2018/04/preschool-art.jpg" width="30%">

Children are at the very basic stage of their learning and would have started with simple reading and writing. The content is designed in such a way so that they can get started easily.

The topics include : 
1. **Introduction**
This is a part of teaching students the basic sentence structure by teaching them to introduce themselves . They will be taught to tell basic information around them to people around them.
 
2. **Alphabet Dictionary**
In order to make learning fun, the trail will contain attractive flash cards for the alphabets along with an audio, which speaks out the letter. 
<img src="https://images-na.ssl-images-amazon.com/images/I/91UaDj%2BYmDL._SL1500_.jpg" width = "200">

3. **Rhyming words**
Simple rhyming words ending with -at, -all, -an, -ar, -ot

4. **Similar Words**
To teach children about words with same meaning.

5. **Opposite Words**
To teach children about words with different meaning.

<img src="https://image.shutterstock.com/image-vector/illustration-kids-sitting-class-singing-260nw-1555022885.jpg" width = "200">


> **GROUP 2 : KINDERGARTNERS**

<img src="https://www.sheknows.com/wp-content/uploads/2018/08/kindergarten-students-feature.jpeg?w=695&h=391&crop=1" width = "200">

Kids of this group are capable of learning advanced english . So for them we have decided on a simple quiz and content appropriate for their age like short stories, differentiating objects etc.

The topics include : 
1. **Introduction** 
This is a part of teaching students the basic sentence structure by teaching them to introduce themselves . They will be taught to tell basic information around them to people around them.

2. **Naming Words**
A noun is the name of a place, person, animal or thing. This part allows children to identify and differentiate nouns from other figures of speech.
<img src="https://www.anglomaniacy.pl/img/g-nouns-singular.png" width = "200">

3. **Describing words**
An adjective is a word that describes a noun. This part allows children to identify and differentiate adjectives from other figures of speech.
<img src="https://image.freepik.com/free-vector/set-children-doing-activities-with-adjectives_1308-39960.jpg" width = "200">

4. **Action Words**
A verb is  an action word. This part allows children to identify and differentiate verbs from other figures of speech.
<img src="https://2ycpbg126nun4c78uq3xemiu-wpengine.netdna-ssl.com/wp-content/uploads/monkey-verbs-800x591.jpg" width = "200">

5. **Singular - Plural**
The purpose of this exercise is to explain  children the  concept of one and many.

6. **Vowels**
The aim of this exercise is to differentiate between vowels and constant.
<img src="https://i.ytimg.com/vi/b4JBqIkiXvA/maxresdefault.jpg" width = "200">

<hr>
