import 'package:flutter_tts/flutter_tts.dart';

final FlutterTts flutterTts = FlutterTts();

speak_big() async{
  await flutterTts.setLanguage("en-IN");
  await flutterTts.setSpeechRate(1);
  await flutterTts.speak("I live in a BIG house! Big is a Describing Word!");
}

speak_pink() async{
  await flutterTts.setLanguage("en-IN");
  await flutterTts.setSpeechRate(1);
  await flutterTts.speak("I love my bow! My bow is pink in colour! All colours are Describing Words.");
}

speak_sweet() async{
  await flutterTts.setLanguage("en-IN");
  await flutterTts.setSpeechRate(1);
  await flutterTts.speak("I have a sweet voice and I love to sing! Sweet is a Describing Word.");
}

speak_tasty() async{
  await flutterTts.setLanguage("en-IN");
  await flutterTts.setSpeechRate(1);
  await flutterTts.speak("My banana is very tasty! Tasty is a Describing Word!");
}

speak_name() async{
  await flutterTts.setLanguage("en-IN");
  await flutterTts.setSpeechRate(1);
  await flutterTts.speak("Hello! My name is Donut, and I am a Dog! All names are Naming Words.");
}

speak_place() async{
  await flutterTts.setLanguage("en-IN");
  await flutterTts.setSpeechRate(1);
  await flutterTts.speak("I live in my house! My house is beautiful! All places are Naming Words.");
}

speak_thing() async{
  await flutterTts.setLanguage("en-IN");
  await flutterTts.setSpeechRate(1);
  await flutterTts.speak("This is my bone! My bone is my favourite thing! All things are Naming Words.");
}

speak_boy() async{
  await flutterTts.setLanguage("en-IN");
  await flutterTts.setSpeechRate(1);
  await flutterTts.speak("This is Lily! She is my best friend! Lily is a girl! Girl is a Naming Word.");
}