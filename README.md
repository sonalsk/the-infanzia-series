# Interactive Learning Applications for Kids: THE INFANZIA SERIES


“The Infanzia Series” is a series of interactive learning mobile applications for pre-schlooers and kindergartners. The main idea behind these applications is to create a fun and interactive learning environment for kids.

### SERIES CONTENT

The Infanzia Series will contain individual applications for areas which are as follows:

- English and Language : word-it-kid
- Numbers and Math : count-it-kid
- Environmental Studies : know-it-kid

<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/wik.png" width = 20%>
<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/cik.png" width = 20%>
<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/kik.png" width = 20%>

### BASIC STRUCTURE


> **CONTENT DIVISION**

Based on the age group, we have divided the content into two categories, i.e., Pre-Schoolers and Kindergartners.  The content, including the exercises, will be designed in such a way that it goes well with the child and his age.

**We have divided the users into 2 groups and have designed our content based on it.**

1. **Group 1 -> PRESCHOOLERS**

Children are at the very basic stage of their learning now. Many of them would have started with basic reading and writing. Content will include videos like rhymes, alphabets, numbers and colors, so they can get started easily.

2. **Group 2 -> KINDERGARTNER**

Kids of this age group are capable of learning advanced ideas because they can recognize similar patterns. So for them we have decided on a simple quiz and content appropriate for their age like short stories, differentiating objects etc.

> **REDIRECTION AND CONTENT**

Upon login the user will be asked the kid’s name and the age. They’ll be prompted to two different interfaces based on the group selected. 
After selecting the group the kids will be directed to the main page.The main page will be divide into three parts : 

<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/wik-login.jpeg" width = 20%>
<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/cik-login.jpeg" width = 20%>
<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/kik-login.jpeg" width = 20%>

**EDUCATIONAL CONTENT**

The kids will go through content and then they’ll be prompted to games like quizzes to imbibe their learning.

<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/wik-content.png" width = 30%>
<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/cik-content.png" width = 30%>
<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/kik-content.png" width = 30%>

**GAMES**

To make it a fun learning experience, we have decided to keep certain games that are educational and relevant to the subject. They will be appropriate for all the age groups using the app. 

<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/games.png" width = 45%>

**RHYMES**

Rhymes are one of most memorable parts of early learning. In order to make this learning process memorable and fun, we will be including certain rhymes and poems for both age groups, combined.

<img src="https://gitlab.com/sonalsk/the-infanzia-series/-/raw/master/SnapShots/rhymes.png" width = 45%>

<hr>